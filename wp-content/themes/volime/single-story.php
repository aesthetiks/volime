<?php 
    get_header();
?>

<?php 
    while(have_posts()){
    the_post(); 
?>
        <div class="blog-hero single" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID(),'thumbnail') ?>') no-repeat; "></div>

        <div class="single-story-box" data-id="<?php echo get_the_ID() ?>">
            <div class="single-story-header">
                <div class="story-user-image" style="background: url('<?php the_field('social_media_avatar') ?>')"></div>
                <div class="story-user-details">
                    <p class="user-name"><?php the_field('social_media_username')?></p>
                    <p class="user-followers">Followers: <span class="user-followers-count"><?php the_field('number_of_followers') ?><span></p>
                    <p class="user-followers">Posts: <span class="user-followers-count"><?php the_field('number_of_posts') ?><span></p>
                </div>
                <a href="<?php the_field('social_media_link')?>" class="story-user-link w150">Visit Profile</a>
            </div>

            <div class="single-story-content">
                <div class="content-block">
                    <h4 class="content-title"><?php the_field('intro_title') ?></h4>
                    <p class="content-text"><?php the_field('intro') ?></p>
                </div>

                <div class="content-block chart">
                    <h4 class="content-title">Before we Started</h4>
                    <h4 class="content-title">After we Finished</h4>
                    <div class="chart-block chart-before">
                        <svg width="342px" height="288px" viewBox="0 0 342 288" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <g id="chart1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="triangle-before" transform="translate(0.000000, -1.000000)">
                                    <g id="group-before">
                                        <polygon id="outline-before" stroke="#F0F0F0" fill-rule="nonzero" points="170 22 312.894192 269.5 27.1058084 269.5"></polygon>
                                        <text id="followers-before" font-family="Roboto-Light, Roboto" font-size="10" font-weight="300" fill="#7B8C8C">
                                            <tspan x="149" y="9">followers</tspan>
                                        </text>
                                        <text id="impressions-before" font-family="Roboto-Light, Roboto" font-size="10" font-weight="300" fill="#7B8C8C">
                                            <tspan x="0" y="279">impressions</tspan>
                                        </text>
                                        <text id="engagement-before" font-family="Roboto-Light, Roboto" font-size="10" font-weight="300" fill="#7B8C8C">
                                            <tspan x="282" y="279">engagements</tspan>
                                        </text>
                                        <path d="M170,22 L170,182.5" id="y-before" stroke="#F0F0F0" stroke-linecap="square" fill-rule="nonzero"></path>
                                        <path d="M27.1058084,269.5 L170,182.5" id="z-before" stroke="#F0F0F0" stroke-linecap="square" fill-rule="nonzero"></path>
                                        <path d="M312.894192,269.5 L170,182.5" id="x-before" stroke="#F0F0F0" stroke-linecap="square" fill-rule="nonzero"></path>
                                        <polygon id="bg-chart-before" fill-opacity="0.5" fill="#AF7BB4" fill-rule="nonzero" points="170 170 181 189 159 189"></polygon>
                                        <polygon id="main-chart-before" fill-opacity="0.5" fill="#AF7BB4" fill-rule="nonzero" points="170 170 181 189 159 189"></polygon>
                                        <text id="followers-count-before" font-family="Roboto-Bold, Roboto" font-size="8" font-weight="bold" fill="#AF7BB4">
                                            <tspan x="162" y="18">0</tspan>
                                        </text>
                                        <text id="impressions-count-before" font-family="Roboto-Bold, Roboto" font-size="8" font-weight="bold" fill="#AF7BB4">
                                            <tspan x="19" y="288">0</tspan>
                                        </text>
                                        <text id="engagement-count-before" font-family="Roboto-Bold, Roboto" font-size="8" font-weight="bold" fill="#AF7BB4">
                                            <tspan x="305" y="288">0</tspan>
                                        </text>
                                    </g>
                                </g>
                            </g>

                        </svg>
                    </div>
                    <div class="chart-block chart-after">
                        <svg width="342px" height="288px" viewBox="0 0 342 288" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <g id="chart2" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="triangle-after" transform="translate(0.000000, -1.000000)">
                                    <g id="group-after">
                                        <polygon id="outline-after" stroke="#F0F0F0" fill-rule="nonzero" points="170 22 312.894192 269.5 27.1058084 269.5"></polygon>
                                        <text id="followers-after" font-family="Roboto-Light, Roboto" font-size="10" font-weight="300" fill="#7B8C8C">
                                            <tspan x="149" y="9">followers</tspan>
                                        </text>
                                        <text id="impressions-after" font-family="Roboto-Light, Roboto" font-size="10" font-weight="300" fill="#7B8C8C">
                                            <tspan x="0" y="279">impressions</tspan>
                                        </text>
                                        <text id="engagement-after" font-family="Roboto-Light, Roboto" font-size="10" font-weight="300" fill="#7B8C8C">
                                            <tspan x="282" y="279">engagements</tspan>
                                        </text>
                                        <path d="M170,22 L170,182.5" id="y-after" stroke="#F0F0F0" stroke-linecap="square" fill-rule="nonzero"></path>
                                        <path d="M27.1058084,269.5 L170,182.5" id="z-after" stroke="#F0F0F0" stroke-linecap="square" fill-rule="nonzero"></path>
                                        <path d="M312.894192,269.5 L170,182.5" id="x-after" stroke="#F0F0F0" stroke-linecap="square" fill-rule="nonzero"></path>
                                        <polygon id="bg-chart-after" fill-opacity="0.5" fill="#4FA6A5" fill-rule="nonzero" points="170 170 181 189 159 189"></polygon>
                                        <polygon id="main-chart-after" fill-opacity="0.5" fill="#4FA6A5" fill-rule="nonzero" points="170 170 181 189 159 189"></polygon>
                                        <text id="followers-count-after" font-family="Roboto-Bold, Roboto" font-size="8" font-weight="bold" fill="#4FA6A5">
                                            <tspan x="162" y="18">0</tspan>
                                        </text>
                                        <text id="impressions-count-after" font-family="Roboto-Bold, Roboto" font-size="8" font-weight="bold" fill="#4FA6A5">
                                            <tspan x="19" y="288">0</tspan>
                                        </text>
                                        <text id="engagement-count-after" font-family="Roboto-Bold, Roboto" font-size="8" font-weight="bold" fill="#4FA6A5">
                                            <tspan x="305" y="288">0</tspan>
                                        </text>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </div>
                </div>

                <div class="content-block">
                    <h4 class="content-title"><?php the_field('description_title') ?></h4>
                    <p class="content-text"><?php the_field('description') ?></p>
                </div>

                <div class="content-block story-image-grid">
                    <?php
                        $count = 1;
                        while($count < 10){

                            $image = get_field('grid_image_' . $count);
                            $size = 'thumbnail';
                            $thumb = $image['sizes'][ $size ];

                            ?> 
                                <div class="image-grid image" style="background:url(<?php echo $thumb ?>)"></div>
                            <?php
                            $count++;
                        }
                    ?>
                </div>

                <div class="content-block">
                    <h4 class="content-title"><?php the_field('conclusion_title') ?></h4>
                    <p class="content-text"><?php the_field('conclusion') ?></p>
                </div>

            </div>
        </div>
<?php
    }
    include(locate_template('components/latest-blogs.php')); 
    get_footer();
?>