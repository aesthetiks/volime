<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>
<body>
<div class="modal-backdrop"></div>
    <div class="side-bar">
        <?php
            include(locate_template('components/social-media.php'));  
        ?>   
    </div>
    <header class="box">
        <a href="<?php echo site_url() ?>" class="logo"><h1>Volime</h1></a>
        <div class="logo-line"></div>

        <nav class="main-nav">
        <?php 
            wp_nav_menu(array(
                'theme_location' => 'headerMenu'
            ));
        ?>
        </nav>

        <?php
            include(locate_template('components/subscribe.php'));  
        ?>   
    </header>