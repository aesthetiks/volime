<?php 
    get_header();
?>

<?php 
    while(have_posts()){
    the_post(); 
?>
    <div class="blog-hero single" style="background: url('<?php echo get_the_post_thumbnail_url() ?>') no-repeat; "></div>
        <div class="single-story-box">
        <h1 class="archive-title single"><?php the_title(); ?></h1>
        <span class="latest-blog-post-date"><?php the_date();?></span>
        <div class="blog-tags single">
            <ul class="tag-list">
                <?php
                $tags = get_tags();
                if ( $tags ) :
                    foreach ( $tags as $tag ) : ?>
                        <li><a href="<?php echo esc_url( get_tag_link( $tag->term_id ) ); ?>" title="<?php echo esc_attr( $tag->name ); ?>"><?php echo esc_html( $tag->name ); ?></a></li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>
        <div class="blog-box-single">
            <div class="blog-content single">
                <div class="content-text">
                    <?php the_content(); ?>
                </div>  
            </div>
        </div>
    <?php
        }
        ?>       
    </div>
    
<?php
    get_footer();
?>