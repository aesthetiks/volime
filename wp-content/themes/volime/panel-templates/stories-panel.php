<div class="stories-panel box">
    <h2 class="main-text">Take a look at the stories we created</h2>
    <div class="stories-box flex-type-1">

        <?php 
            $homepageStories = new WP_Query(array(
                'posts_per_page' => 8,
                'post_type' => 'story'
            ));

            while($homepageStories->have_posts()){
                $homepageStories->the_post(); 

                $image = get_field('social_media_avatar');
                $size = 'medium';
	            $thumb = $image['sizes'][ $size ];
        ?>
        
            <div class="story-box" style="background:url('<?php echo get_the_post_thumbnail_url(get_the_ID(),'thumbnail') ?>')">
                <div class="story-details">
                    <div class="story-user-image" style="background: url('<?php echo $thumb ?>')"></div>
                    <div class="story-user-details">
                        <p class="user-name"><?php the_field('social_media_username')?></p>
                        <p class="user-followers">Followers: <span class="user-followers-count highlight"><?php the_field('number_of_followers') ?></span></p>
                    </div>
                    <a href="<?php the_permalink()?>" class="story-user-link">Full Story</a>
                </div>
            </div>
            
        <?php
        } wp_reset_postdata();
        ?> 
        
    </div>
</div>