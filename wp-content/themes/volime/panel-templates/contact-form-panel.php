<div id="contact-us" class="tagline-panel contact box h-auto">

    <h2 class="main-text">Say hi, give us a wink</h2>
    <h3 class="highlight-text mb100">Or we can talk business</h3>

    <div class="contact-box">
        <div>
            <input type="text" placeholder="NAME" name="cname" class="cname">
            <span class="error-message">Field is required</span>
        </div>
        <div>
            <input type="text" placeholder="TITLE" name="ctitle" class="ctitle">
            <span class="error-message">Field is required</span>
        </div>
        <div>
            <input type="text" placeholder="COMPANY" name="ccompany" class="ccompany">
            <span class="error-message">Field is required</span>
        </div>
        <div>
            <input type="text" placeholder="EMAIL" name="cemail" class="cemail">
            <span class="error-message">Field is required</span>
        </div>
        <div>
            <input type="text" placeholder="PHONE NUMBER" name="cphone" class="cphone">
            <span class="error-message">Field is required</span>
        </div>
        <div>
            <input type="text" placeholder="DESIRED START DATE" name="cdate" class="cdate datepicker" readonly="readonly">
            <span class="error-message">Field is required</span>
        </div>
        <div class="range-slider">
            <label for="estimatedBudget">Estimated Budget</label>
            <input id="estimatedBudget" class="range-slider__range cbudget" name="cbudget" type="range" value="240" min="100" max="1000" step="10">
        </div>
        <div class="range-placeholder">
            <span class="estimated-budget">$ <span class="range-slider__value">240</span> / mo</span>
        </div>
        <div class="range-placeholder"></div>
        <div class="message-box">
            <textarea placeholder="MESSAGE" name="cmessage" class="cmessage"></textarea>
            <span class="error-message">Field is required</span>
        </div>
        <div class="contact-btn-box">    
            <button type="button" class="contact-btn">Send</button>
            <span class="contact-btn-mid">or<span class="contact-btn-hi">Say Hi</span></span>
        </div>
        <div class="confirm">
            <div>
                <input type="checkbox" name="checkbox" id="confirmContact" value="value">
                <label for="confirmContact" class="contact-form-text">I've read and accept the <a href="<?php site_url(); ?>/terms-of-service">Terms of Service</a></label>
            </div>
        </div>
    </div>

</div>