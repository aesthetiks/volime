<?php 
    $homepageTaglines = new WP_Query(array(
        'posts_per_page' => 1,
        'post_type' => 'tagline'
    ));

    while($homepageTaglines->have_posts()){
        $homepageTaglines->the_post(); 
?>

<div class="tagline-panel box">
    <h2 class="main-text">
        <?php the_field('main_tagline')?>
    </h2>
    <h3 class="highlight-text"><?php the_field('highlight_tagline')?></h3>
    <ul class="tagline-services-list">
        <li>
            <p><?php the_field('service_1_main')?></p>
            <p class="sub"><?php the_field('service_1_subline')?></p>
        </li>
        <li>
            <p><?php the_field('service_2_main')?></p>
            <p class="sub"><?php the_field('service_2_subline')?></p>
        </li>
        <li>
            <p><?php the_field('service_3_main')?></p>
            <p class="sub"><?php the_field('service_3_subline')?></p>
        </li>
        <li>
            <p><?php the_field('service_4_main')?></p>
            <p class="sub"><?php the_field('service_4_subline')?></p>
        </li>
    </ul>
</div>

<?php
    } wp_reset_postdata();
?>  