<div id="services" class="tagline-panel services box">
    <?php 
        $mainService = new WP_Query(array(
            'posts_per_page' => 1,
            'post_type' => 'main-service'
        ));

        while($mainService->have_posts()){
            $mainService->the_post(); 
    ?>

    <h2 class="main-text"><?php the_field('main_tagline')?></h2>
    <h3 class="highlight-text mb100"><?php the_field('highlight_tagline')?></h3>
        
    <div class="services-box">

    <?php

        $services = get_field('services');
    
        foreach($services as $post){
            setup_postdata($post);
            $orgTitle = get_field('title');
            $title = str_replace(' ', '', $orgTitle);
    ?>  

        <div class="service-box">
            <div class="service-header">
                <div>
                    <h4 class="service-title"><?php echo get_field('title');?></h4>
                    <p class="service-subtitle">BUNDLE</p>
                </div>
                
                <div class="service-icons">
                <?php 
                    for ($x = 1; $x < 4; $x++){
                        if(get_field('icon_'.$x) != "" && get_field('icon_'.$x) != "none"){
                ?>
                        <span class="service-icon si-<?php echo get_field('icon_'.$x);?>"></span>
                <?php
                    }
                }
                ?>
                </div>
            </div>
                    
            <p class="service-description"><?php echo get_field('description');?></p>
            <ul class="service-list">

            <?php 
                for ($x = 1; $x < 7; $x++){
                    if(get_field('item_'.$x) != ""){
            ?>
                    <li><p><?php echo get_field('item_'.$x);?></p></li>
            <?php
                    }
                }
            ?>  
            </ul>
                    
            <div class="price-group">
                <span>$<span class="price"><?php echo get_field('price');?></span>/<span class="price-value"><?php echo get_field('price_value');?></span></span>
            </div>
            <button type="button" class="service-btn <?php echo strtolower($title)?> ">Choose</button>      
            
            <?php
                include( locate_template( 'components/service-modal.php', false, false ) ); 
            ?>

        </div>       
    <?php
        }
            } wp_reset_postdata();
    ?> 
    </div> 
</div>