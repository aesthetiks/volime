<?php 
    get_header();
?>

<div class="blog-hero single"></div>
        <div class="single-story-box">
        <h1 class="archive-title">Terms of Service</h1>

        <div class="blog-box-single">
            <div class="blog-content single">
                <div class="content-text">
                    <h2><strong>1. Terms</strong></h2>
                    <p>By accessing the website at <a href="http://www.volime.club">www.volime.club</a>, you are agreeing to be bound by these terms of service, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this website are protected by applicable copyright and trademark law.
                    </p><br/><br/>
                    <h2><strong>2. Use License</strong></h2>
                    <ol type="1" class="pl-14">
                        <li>
                        Permission is granted to temporarily download one copy of the materials (information or software) on Volime Club's website for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:
                        modify or copy the materials;
                            <ol type="i" class="pl-14">
                                <li>
                                    use the materials for any commercial purpose, or for any public display (commercial or non-commercial);
                                </li>
                                <li>
                                    attempt to decompile or reverse engineer any software contained on Volime Club's website;
                                </li>
                                <li>
                                    remove any copyright or other proprietary notations from the materials; or
                                </li>
                                <li>
                                    transfer the materials to another person or "mirror" the materials on any other server.
                                </li>
                            </ol>
                        </li><br/>
                        <li>
                            This license shall automatically terminate if you violate any of these restrictions and may be terminated by Volime Club at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.
                        </li>
                    </ol><br/><br/>

                    <h2><strong>3. Disclaimer</strong></h2>
                    <p>The materials on Volime Club's website are provided on an 'as is' basis. Volime Club makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights.
                    Further, Volime Club does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its website or otherwise relating to such materials or on any sites linked to this site.
                    </p><br/><br/>

                    <h2><strong>4. Limitations</strong></h2>
                    <p>
                    In no event shall Volime Club or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption) arising out of the use or inability to use the materials on Volime Club's website, even if Volime Club or a Volime Club authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.
                    </p><br/><br/>

                    <h2><strong>5. Accuracy of materials</strong></h2>
                    <p>
                    The materials appearing on Volime Club's website could include technical, typographical, or photographic errors. Volime Club does not warrant that any of the materials on its website are accurate, complete or current. Volime Club may make changes to the materials contained on its website at any time without notice. However Volime Club does not make any commitment to update the materials.
                    </p><br/><br/>

                    <h2><strong>6. Links</strong></h2>
                    <p>
                    Volime Club has not reviewed all of the sites linked to its website and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Volime Club of the site. Use of any such linked website is at the user's own risk.
                    </p><br/><br/>

                    <h2><strong>7. Modifications</strong></h2>
                    <p>
                    Volime Club may revise these terms of service for its website at any time without notice. By using this website you are agreeing to be bound by the then current version of these terms of service.
                    </p><br/><br/>

                    <h2><strong>8. Governing Law</strong></h2>
                    <p>
                    These terms and conditions are governed by and construed in accordance with the laws of Bosnia & Herzegovina and you irrevocably submit to the exclusive jurisdiction of the courts in that State or location.
                    </p>      
                </div>  
            </div>
        </div>
     
    </div>

<?php
    get_footer();
?>