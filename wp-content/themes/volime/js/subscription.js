jQuery(document).ready(function( $ ) {
$('form.subscribe').submit(function(e) {
    var $this = $(this);
    if(subscribeFormValidation($this)){
    if($('#confirmSubscription').is(':checked')){
        $.ajax({
            type: 'GET', // GET & url for json slightly different
            url: 'https://club.us20.list-manage.com/subscribe/post-json?c=?',
            data: $this.serialize(),
            dataType    : 'json',
            contentType: 'application/json; charset=utf-8',
            error       : function(err) { 
                $('.info-modal-header').html('Ups!');
                    $('.info-modal-text').html('Something went wrong. We can\'t currently add you to our Newsletter list.');
                    $('.info-modal-icon').addClass('subscription');
                    $('.info-modal').addClass('show');
                    $('.modal-backdrop').addClass('show');
                    setTimeout(function() { 
                        $('.info-modal-icon').removeClass('subscription');
                        $('.info-modal').removeClass('show');
                        $('.modal-backdrop').removeClass('show');
                    }, 10000);
            },
            success     : function(data) {
                if (data.result != 'success') {
                    console.log(data);
                    // Something went wrong, parse data.msg string and display message
                    $('.info-modal-header').html('Hey there!');
                    $('.info-modal-text').html('This Email is already subscribed to our Newsletter.');
                    $('.info-modal-icon').addClass('subscription');
                    $('.info-modal').addClass('show');
                    $('.modal-backdrop').addClass('show');
                    setTimeout(function() { 
                        $('.info-modal-icon').removeClass('subscription');
                        $('.info-modal').removeClass('show');
                        $('.modal-backdrop').removeClass('show');
                    }, 10000);
                } else {
                    // It worked, so hide form and display thank-you message.
                    $('.modal-backdrop').addClass('show');
                    $('.info-modal-header').html('Welcome!');
                    $('.info-modal-text').html('Thank you for choosing to be a part of the Volime Club. You\'ll be among the first to get our news and updates.');
                    $('.info-modal-icon').addClass('subscription');
                    $('.info-modal').addClass('show');
                    $('.modal-backdrop').addClass('show');
                    setTimeout(function() { 
                        $('.info-modal-icon').removeClass('subscription');
                        $('.info-modal').removeClass('show');
                        $('.modal-backdrop').removeClass("show");
                    }, 10000);
                }
            }
        });
        
        $('.subscribe-field').val('');
        $('#confirmSubscription').prop('checked', false);
        $('.subscribe-text').removeClass('error');

        
    }else{
        e.preventDefault();
        $($this).find('.subscribe-text').addClass('error');
    }
    }

    return false;
  });


  function subscribeFormValidation($this){
    var validated = true;

    //Required validation
    if(!$($this).find('.subscribe-field').val()){
      $($this).find('.subscribe-field').addClass('is-required');
      validated = false;
    }else{
      if(!isValidEmailAddress($($this).find('.subscribe-field').val())){
        $($this).find('.subscribe-field').addClass('is-required invalid-email');
        validated = false;
      }else{
        $($this).find('.subscribe-field').removeClass('is-required invalid-email');
      }  
    }
    return validated;
  }

  function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
  };

  setTimeout(function(){
    $('body').addClass('fade-in');
  }, 300);

  if(/Mobi/i.test(navigator.userAgent) || /Android/i.test(navigator.userAgent)){
    $('head').append('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" /> ');
    $('body').addClass('mobile');
  } 

  var $document = $(document),
  $element = $('header'),
  className = 'min';

  $document.scroll(function() {
  $element.toggleClass(className, $document.scrollTop() >= 550);
  });

  $('.mobile-subscribe-btn').click(function(){
      $('.subscribe').toggleClass('mobile-subscribe');
      $(this).toggleClass('open');
  });

  $(document.body).on("focus", "textarea, input:not(.subscribe-field)", function() {
      $(document.body).addClass("keyboard");
   }).on("blur", "textarea, input", function() {
      $(document.body).removeClass("keyboard");
   });;

});