jQuery(document).ready(function( $ ) {

    //$( '.datepicker' ).datepicker({ dateFormat: 'd MM, yy' });

    $('.contact-btn').click(function(e) {

      if(contactFormValidation()){  
        if($('#confirmContact').is(':checked')){
        
          jQuery.ajax({   
            url:ajax_contact.ajax_url,
            type:'POST',
            dataType:'json',
            data: {
                action :'contact_insert',
                security: ajax_contact.ajax_nonce,
                cname : $('.contact-box .cname').val(),
                ctitle : $('.contact-box .ctitle').val(),
                ccompany : $('.contact-box .ccompany').val(),
                cemail : $('.contact-box .cemail').val(),
                cphone : $('.contact-box .cphone').val(),
                cdate : $('.contact-box .cdate').val(),
                cbudget : $('.contact-box .cbudget').val(),
                cmessage : $('.contact-box .cmessage').val(),
                
            },
            success: function(response){
              $('.info-modal-header').html('Thank You!');
              $('.info-modal-text').html('We will be reaching out to you within a couple of hours. In the meantime, enjoy going through our stories and blogs. If you have any additional questions, please feel free to say hi, or have a chat with us on any of our social media platforms.');
              $('.info-modal-icon').addClass('mail');
              $('.modal-backdrop').addClass('show');
              $('.info-modal').addClass('show');
              setTimeout(function() { 
                $('.info-modal-icon').removeClass('mail');
                $('.info-modal').removeClass('show');
                $('.modal-backdrop').removeClass('show');
              }, 10000);

              $('.contact-box .cname').val('');
              $('.contact-box .ctitle').val('');
              $('.contact-box .ccompany').val('');
              $('.contact-box .cemail').val('');
              $('.contact-box .cphone').val('');
              $('.contact-box .cdate').val('');
              $('.contact-box .cbudget').val('240');
              $('.range-slider__value').html('240');
              $('.contact-box .cmessage').val('');
            },
            error: function(response){
              $('.info-modal-header').html('Ups!');
              $('.info-modal-text').html('Something went wrong while processing your request. Your details are still there so you can try one more time.');
              $('.info-modal-icon').addClass('mail');
              $('.modal-backdrop').addClass('show');
              $('.info-modal').addClass('show');
              setTimeout(function() { 
                $('.info-modal-icon').removeClass('mail');
                $('.info-modal').removeClass('show');
                $('.modal-backdrop').removeClass('show');
              }, 10000);
            }  
          }); 
          
          $('#confirmContact').prop('checked', false);
          $('.contact-form-text').removeClass('error');

        }else{
          e.preventDefault();
          $('.contact-form-text').addClass('error');
        }
      }
  
      return false;
    });

    $('.range-slider__range').on('input', function(){
      $('.range-slider__value').html(this.value);
    });


    function contactFormValidation(){
      var validated = true;

      //Required validation
      if(!$('.cname').val()){
        $('.cname').addClass('is-required').next().addClass('error');
        validated = false;
      }else{
        $('.cname').removeClass('is-required').next().removeClass('error');
      }
      if(!$('.ctitle').val()){
        $('.ctitle').addClass('is-required').next().addClass('error');
        validated = false;
      }else{
        $('.ctitle').removeClass('is-required').next().removeClass('error');
      }
      if(!$('.ccompany').val()){
        $('.ccompany').addClass('is-required').next().addClass('error');
        validated = false;
      }else{
        $('.ccompany').removeClass('is-required').next().removeClass('error');
      }
      if(!$('.cemail').val()){
        $('.cemail').addClass('is-required').next().addClass('error');
        validated = false;
      }else{
        if(!isValidEmailAddress($('.cemail').val())){
          $('.cemail').addClass('is-required').next().html('Invalid email format').addClass('error');
          validated = false;
        }else{
          $('.cemail').removeClass('is-required').next().html('Field is required').removeClass('error');
        }  
      }
      if(!$('.cphone').val()){
        $('.cphone').addClass('is-required').next().addClass('error');
        validated = false;
      }else{
        if(!isValidPhoneNumber($('.cphone').val())){
          $('.cphone').addClass('is-required').next().html('Invalid phone number').addClass('error');
          validated = false;
        }else{
          $('.cphone').removeClass('is-required').next().html('Field is required').removeClass('error');
        }
      }
      if(!$('.cdate').val()){
        $('.cdate').addClass('is-required').next().addClass('error');
        validated = false;
      }else{
        $('.cdate').removeClass('is-required').next().removeClass('error');
      }
      if(!$('.cmessage').val()){
        $('.cmessage').addClass('is-required').next().addClass('error');
        validated = false;
      }else{
        $('.cmessage').removeClass('is-required').next().removeClass('error');
      }

      return validated;
    }


    function isValidEmailAddress(emailAddress) {
      var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
      return pattern.test(emailAddress);
    };

    function isValidPhoneNumber(phoneNumber){
      var pattern = new RegExp(/^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i);
      return pattern.test(phoneNumber);
    }
  
});