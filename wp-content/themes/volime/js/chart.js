jQuery(document).ready(function( $ ) {
    postID = $('.single-story-box').attr('data-id');

    $.getJSON('//volime.club/wp-json/wp/v2/stories-api/' + postID, story => {
        $('#followers-count-before > tspan').html(story.story_details.followers_before[0]);
        $('#impressions-count-before > tspan').html(story.story_details.impressions_before[0]);
        $('#engagement-count-before > tspan').html(story.story_details.engagement_before[0]);
        
        var highest = bigger(story.story_details.followers_before[0], story.story_details.impressions_before[0], story.story_details.engagement_before[0], story.story_details.followers_after[0], story.story_details.impressions_after[0], story.story_details.engagement_after[0]);
        setChart(story.story_details.followers_before[0], story.story_details.engagement_before[0], story.story_details.impressions_before[0], highest, 'before');
        setChart(story.story_details.followers_after[0], story.story_details.engagement_after[0], story.story_details.impressions_after[0], highest, 'after');


        $('#followers-count-after > tspan').html(story.story_details.followers_after[0]);
        $('#impressions-count-after > tspan').html(story.story_details.impressions_after[0]);
        $('#engagement-count-after > tspan').html(story.story_details.engagement_after[0]);
    });


    function setChart(x,y,z, biggest, chart){
        //main chart
        var followersY = 183 - (x * (149 / (biggest + 1)));    
        var engagementX = 170 + (y * (143 / (biggest + 1)));
        var engagementY = 183 + (y * (87 / (biggest + 1)));
        var impressionsX = 170 - (z * (143 / (biggest + 1)));
        var impressionsY = 183 + (z * (87 / (biggest + 1)));

        //bg chart
        var followersYbg = 183 - (x * (149 / (biggest*2 + 1)));    
        var engagementXbg = 170 + (y * (143 / (biggest*2 + 1)));
        var engagementYbg = 183 + (y * (87 / (biggest*2 + 1)));
        var impressionsXbg = 170 - (z * (143 / (biggest*2 + 1)));
        var impressionsYbg = 183 + (z * (87 / (biggest*2 + 1)));

        $('#main-chart-' + chart).attr('points', '170 ' + followersY + ' ' + engagementX + ' ' + engagementY + ' ' + impressionsX + ' ' + impressionsY);
        $('#bg-chart-' + chart).attr('points', '170 ' + followersYbg + ' ' + engagementXbg + ' ' + engagementYbg + ' ' + impressionsXbg + ' ' + impressionsYbg);
    
    }
    
    function bigger(x,y,z,a,b,c) {
        var biggest = Math.max(x,y,z,a,b,c);
        console.log(biggest);
        return biggest;
    }

});




