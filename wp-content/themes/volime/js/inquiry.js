jQuery(document).ready(function( $ ) {

  $('.submit-btn').click(function(e) {

    if(inquiryFormValidation()){
    
      jQuery.ajax({   
        url:ajax_inquiry.ajax_url,
        type:'POST',
        dataType:'json',
        data: {
            action :'inquiry_insert',
            security: ajax_inquiry.ajax_nonce,
            sname : $('.service-modal.show .sname').val(),
            semail : $('.service-modal.show .semail').val(),
            smessage : $('.service-modal.show .smessage').val(),
            sprice : $('.service-modal.show .sprice').val(),
            spackage : $('.service-modal.show .spackage').val(),
        },
        success: function(response){
          $('.service-modal.show .smessage').val(''),
          $('.service-modal').removeClass('show');
          $('.info-modal-header').html('Thank You!');
          $('.info-modal-text').html('We will be reaching out to you within a couple of hours. In the meantime, enjoy going through our stories and blogs. If you have any additional questions, please feel free to say hi, or have a chat with us on any of our social media platforms.');
          $('.info-modal-icon').addClass('mail');
          $('.info-modal').addClass('show');
          setTimeout(function() { 
            $('.info-modal-icon').removeClass('mail');
            $('.info-modal').removeClass('show');
            $(".modal-backdrop").removeClass("show");
          }, 10000);
        },
        error: function(response){
          console.log('error');
          console.log(response.status);
        }  
      });   
    }
    return false;
  });

  function inquiryFormValidation(){
    var validated = true;

    //Email validation
    

    //Required validation
    if(!$('.service-modal.show .sname').val()){
      $('.service-modal.show .sname').addClass('is-required').next().addClass('error');
      validated = false;
    }else{
      $('.service-modal.show .sname').removeClass('is-required').next().removeClass('error');
    }
    if(!$('.service-modal.show .semail').val()){
      $('.service-modal.show .semail').addClass('is-required').next().addClass('error');
      validated = false;
    }else{
      if(!isValidInquiryEmailAddress($('.service-modal.show .semail').val())){
        $('.service-modal.show .semail').addClass('is-required').next().html('Invalid email format').addClass('error');
        validated = false;
      }else{
        $('.service-modal.show .semail').removeClass('is-required').next().html('Field is required').removeClass('error');
      }
    }
    if(!$('.service-modal.show .smessage').val()){
      $('.service-modal.show .smessage').addClass('is-required').next().addClass('error');
      validated = false;
    }else{
      $('.service-modal.show .smessage').removeClass('is-required').next().removeClass('error');
    }

    return validated;
  }

  function isValidInquiryEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
  };

});