jQuery(document).ready(function( $ ) {

    $('.contact-btn-hi').click(function(e) {
  
      $('.service-modal.show').addClass('in-progress');
      
      jQuery.ajax({   
        url:ajax_hi.ajax_url,
        type:'POST',
        dataType:'json',
        data: {
            action :'hi_insert',
            security: ajax_hi.ajax_nonce, 
        },
        success: function(response){
          $('.info-modal-header').html('Hi!');
          $('.info-modal-text').html('We appreciate even these little things. We hope you are having a nice day and if you feel stressed take a minute, make a tea and breathe. It will not hurt anyone if you do so.');
          $('.info-modal-icon').addClass('hi');
          $('.modal-backdrop').addClass('show');
          $('.info-modal').addClass('show');
          setTimeout(function() { 
            $('.info-modal-icon').removeClass('hi');
            $('.info-modal').removeClass('show');
            $('.modal-backdrop').removeClass('show');
          }, 10000);
        },
        error: function(response){
          $('.info-modal-header').html('Ups!');
          $('.info-modal-text').html('Something went wrong on our side but we still appreciate even these little things. We hope you are having a nice day and if you feel stressed take a minute, make a tea and breathe. It will not hurt anyone if you do so.');
          $('.info-modal-icon').addClass('hi');
          $('.modal-backdrop').addClass('show');
          $('.info-modal').addClass('show');
          setTimeout(function() { 
            $('.info-modal-icon').removeClass('hi');
            $('.info-modal').removeClass('show');
            $('.modal-backdrop').removeClass('show');
          }, 10000);
        }  
      }); 
  
    return false;
    });
    
  
  });