<?php 
    get_header();
?>

<div class="blog-hero"></div>
<div class="single-story-box">
<h1 class="archive-title">What are we thinking about lately</h1>

    <?php 
        while(have_posts()){
        the_post(); 
    ?>
            <div class="blog-box">
                <a href="<?php the_permalink(); ?>">
                    <div class="blog-thumbnail" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID(),'thumbnail') ?>') no-repeat; ">
                        
                    </div>
                    <div class="blog-content">
                        <h4 class="content-title"><?php the_title(); ?></h4>
                        <div class="content-text blog">
                            <?php 
                                echo mb_strimwidth((trim(get_the_excerpt())), 0, 352, "...");
                            ?>
                        </div>
                        <span class="read-more">Read More</span>      
                    </div>
                </a>
            </div>

            <div class="blog-tags">
                    <ul class="tag-list">
                        <?php
                        $tags = get_the_tags();
                        if ( $tags ) :
                            foreach ( $tags as $tag ) : ?>
                                <li><a href="<?php echo esc_url( get_tag_link( $tag->term_id ) ); ?>" title="<?php echo esc_attr( $tag->name ); ?>"><?php echo esc_html( $tag->name ); ?></a></li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
            </div>
            

    <?php
        }
        ?>
        <div class="pagination">
        <?php
        echo paginate_links();
    ?>
    </div>
</div>

<?php
    get_footer();
?>