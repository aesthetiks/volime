<?php 
    get_header();
?>

<div class="blog-hero single"></div>
        <div class="single-story-box">
        <h1 class="archive-title">Privacy Policy</h1>

        <div class="blog-box-single">
            <div class="blog-content single">
                <div class="content-text">
                    <h2><strong>1. Perosnal Information We Collect</strong></h2>
                    <p>
                        We respect the privacy of everyone who visits our site. 
                        We do not collect personal information about our users without their knowledge and prior consent. 
                        No scripts or devices are used for this purpose. In order to use certain features of our website we 
                        may ask you to register by entering some personal information. This information is given voluntarily 
                        and will be used only as stated in the paragraphs below. This information will not be given, traded or 
                        sold to any third party without the user's knowledge and prior consent.
                    </p><br/><br/>

                    <h2><strong>2. Purpose</strong></h2>
                    <p>
                    Volime Club as the Party Responsible of Data Handling, will handle your personal data for the following purposes:
                        <ol type="1" class="pl-14">
                            <li>
                                Manage your registration as Platform user, to identify you and give you access to Services available for registered Platform users.
                            </li>
                            <li>
                                Contacting you through email regarding updates or information related to features, products or services and updates, as long as they are needed or reasonable.
                            </li>
                            <li>
                                Respond to any request or inquiry you make through the customer service channels available at our Platform.
                            </li>
                            <li>
                                We also use your information to generate aggregated non-identifying data. For instance, generate statistics regarding our users, their jobs or areas of expertise, number of impressions or clicks in a specific project or visitor demographics.
                            </li>
                        </ol>
                    </p><br/><br/>

                    <h2><strong>3. Who obtains your data</strong></h2>
                    <p>
                    To comply with the purposes indicated in this Privacy Policy, it might be necessary to report or release your personal data to third parties for the following reasons:
                    </p>
                        <ul class="pl-14">
                        <li>                
                            Business Transfers: regarding any reorganization, restructuring, merge or sale, or any other transfer of assets, including personal, provided that the receiving party accepts to handle your personal information in accordance with our Privacy Policy.
                        </li>
                        <li>
                            Service Providers: to render services in our name or to help us provide our services. For example, we contract service providers to render marketing, advertising, communication, infrastructure and IT services, to customize and improve our service, to process credit card transactions or other methods of payment, provide customer service, collect debts, analyze and improve data (such as data regarding user interaction with our services), and process and manage satisfaction surveys. For efficiency purposes, these partners and providers may be based in the US or other countries or territories outside of the European Economic Area, and might not provide a data protection level comparable to that of your country, or the European Union's.
                        </li>
                    </ul><br/>
                    <p>
                        When you accept this Privacy Policy, you are specifically authorizing us to handle and report your personal data to the partners mentioned and/or release your personal data to the service providers responsible for data handling, who are outside of the European Economic Area for the purposes described herein and to provide you complete service. When we release personal information when exchanging data with countries outside the European Economic Area and other areas with data protection laws, we will ensure the information is being transferred in compliance with this Privacy Policy and pursuant to current data protection laws.                    
                    </p><br/><br/>

                    <h2><strong>4. Data retention period</strong></h2>
                    <p>
                        We will retain your personal data for as long as we have a valid contractual relationship, and after that, for the period determined by any obligations arising from the treatment of the data or as established by law.
                    </p><br/><br/>

                    <h2><strong>5. Use of your credit card</strong></h2>
                    <p>
                        You may have to provide a credit card to buy products and services from our website. We use third-party billing services and have no control over these services. We use our commercially reasonable efforts to make sure your credit card number is kept strictly confidential by using only third-party billing services that use industry-standard encryption technology to protect your credit card number from unauthorized use. However, you understand and agree that we are in no way responsible for any misuse of your credit card number.

                        If you are paying with PayPal, we will request the email associated to it. If you request an invoice, we will request your invoicing data, if needed.

                        We don’t save your details, but you will be able to modify or delete your credit cards or PayPal account at any time through the payment method in the purchase form or through the payment method setting in your profile.

                        For security reasons, the use of this feature might require changing of your password. Remember, the safety of the Platform also depends on the correct use and storage of confidential passwords.                    </p>
                    </p><br/><br/>

                    <h2><strong>6. Options</strong></h2>
                    <p>
                    You are free to refuse our request for your personal information, with the understanding that we may be unable to provide you with some of your desired services.
                    Your continued use of our website will be regarded as acceptance of our practices around privacy and personal information. If you have any questions about how we handle user data and personal information, feel free to contact us.
                    </p>
                </div>  
            </div>
        </div>
     
    </div>

<?php
    get_footer();
?>