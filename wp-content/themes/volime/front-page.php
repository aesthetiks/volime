<?php 
    get_header();
?>

<?php
    include(locate_template('panel-templates/tagline-panel.php'));  
?> 

<?php
    include(locate_template('panel-templates/stories-panel.php'));  
?> 

<?php
    include(locate_template('panel-templates/services-panel.php'));  
?> 

<?php
    include(locate_template('components/latest-blogs.php')); 
?>

<?php
    include(locate_template('panel-templates/contact-form-panel.php')); 
?>

<?php
    get_footer();
?>
