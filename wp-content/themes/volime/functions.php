<?php 

    date_default_timezone_set('Europe/Berlin');

    //Page import
    function volime_files(){
        wp_enqueue_style('font-oswald', '//fonts.googleapis.com/css?family=Oswald:300,400,500,600');
        wp_enqueue_style('font-roboto', '//fonts.googleapis.com/css?family=Roboto:300,400,500');
        wp_enqueue_style('volime_main_styles', get_template_directory_uri() . '/css/style.min.css', NULL, '1.0');
        wp_enqueue_script('jquery');
        wp_enqueue_script('subscription-js', get_theme_file_uri('/js/subscription.js'), NULL, '1.0', true);  
        if( is_single() && get_post_type()=='story'){
            wp_enqueue_script('volime-js', get_theme_file_uri('/js/chart.js'), NULL, '1.0', true);
        }

        if(is_front_page()){
            wp_enqueue_script('modals-js', get_theme_file_uri('/js/modals.js'), NULL, '1.0', true);    
        }

    }

    add_action('wp_enqueue_scripts', 'volime_files');

    //Page title
    function volime_features(){
        register_nav_menu('headerMenu', 'Header Menu');
        register_nav_menu('footerMenu', 'Footer Menu');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails'); 
    }

    add_action('after_setup_theme', 'volime_features');


    //Rename Post 
    // Replace Posts label as Blog in Admin Panel 

    function change_post_menu_label() {
        global $menu;
        global $submenu;
        $menu[5][0] = 'Blog';
        $submenu['edit.php'][5][0] = 'Blogs';
        $submenu['edit.php'][10][0] = 'Add Blog';
        echo '';
    }

    function change_post_object_label() {
            global $wp_post_types;
            $labels = &$wp_post_types['post']->labels;
            $labels->name = 'Blog Posts';
            $labels->singular_name = 'Blog Post';
            $labels->add_new = 'Add Blog Post';
            $labels->add_new_item = 'Add Blog Post';
            $labels->edit_item = 'Edit Blog Post';
            $labels->new_item = 'Blog Post';
            $labels->view_item = 'View Blog Post';
            $labels->search_items = 'Search Blog Posts';
            $labels->not_found = 'No Blog Posts found';
            $labels->not_found_in_trash = 'No Blog Posts found in Trash';
    }
    
    add_action( 'init', 'change_post_object_label' );
    add_action( 'admin_menu', 'change_post_menu_label' );


    // Inquiries setup
    add_filter('manage_inquiries_posts_columns', 'bs_inquiry_table_head');
    function bs_inquiry_table_head( $defaults ) {
        $defaults['title']  = 'Date';
        $defaults['user_name']  = 'User Name';
        $defaults['email']    = 'Email';
        $defaults['message']   = 'Message';
        $defaults['package'] = 'Package';
        $defaults['price'] = 'Price';
        $defaults['status'] = 'Status';
        return $defaults;
    }

    add_action("manage_inquiries_posts_custom_column",  "inquiries_custom_columns");

    function inquiries_custom_columns($column){
        global $post;

        switch ($column) {
           case "user_name":
               the_field('user_name', $post->ID );
               break;
           case "email":
               the_field('email', $post->ID );
               break;
           case "message":
               the_field('message', $post->ID );
               break;
            case "package":
               the_field('package', $post->ID );
               break;
            case "price":
               the_field('price', $post->ID );
               break;
            case "status":
               the_field('status', $post->ID );
               break;
           }
        }

        function my_enqueue() {
            if(is_front_page()){
                wp_enqueue_script( 'jquery-ui-datepicker' );
                wp_enqueue_script( 'inquiry', get_template_directory_uri() . '/js/inquiry.js', array('jquery') );
                wp_enqueue_script( 'contact', get_template_directory_uri() . '/js/contact-form.js', array('jquery') );
                wp_enqueue_script( 'hi', get_template_directory_uri() . '/js/hi.js', array('jquery') );

                wp_localize_script( 'inquiry', 'ajax_inquiry', 
                array( 
                    'ajax_url' => admin_url( 'admin-ajax.php' ),
                    'ajax_nonce' => wp_create_nonce('inquiry_platform_security') ) );
                wp_localize_script( 'contact', 'ajax_contact', 
                array( 
                    'ajax_url' => admin_url( 'admin-ajax.php' ),
                    'ajax_nonce' => wp_create_nonce('contact_platform_security') ) );
                wp_localize_script( 'hi', 'ajax_hi', 
                array( 
                    'ajax_url' => admin_url( 'admin-ajax.php' ),
                    'ajax_nonce' => wp_create_nonce('hi_platform_security') ) );
            }
        }

        add_action( 'wp_enqueue_scripts', 'my_enqueue' );

        function inquiry_insert() {          

            check_ajax_referer('inquiry_platform_security','security');
        
            $serviceInquiryDate = date('d.m.y h:i');
            $my_cptpost_args = array(
            
                'post_title'    => $serviceInquiryDate,
                'post_content' => 'Empty',
                'post_type' => 'inquiries',
                'post_status'   => 'publish'
            
                );
            // insert the post into the database
            $cpt_id = wp_insert_post( $my_cptpost_args, $wp_error = false);
            update_field('user_name', $_POST['sname'], $cpt_id);
            update_field('email', $_POST['semail'], $cpt_id);
            update_field('message', $_POST['smessage'], $cpt_id);
            update_field('package', $_POST['spackage'], $cpt_id);
            update_field('price', $_POST['sprice'], $cpt_id);
            update_field('status', 'waiting', $cpt_id);

            echo json_encode(array('status' => 'ok'));

            die(''); 
        }

        add_action( 'wp_ajax_inquiry_insert', 'inquiry_insert' );    // If called from admin panel
        add_action( 'wp_ajax_nopriv_inquiry_insert', 'inquiry_insert' );


    // Inbox setup
    add_filter('manage_inbox_posts_columns', 'bs_inbox_table_head');
    function bs_inbox_table_head( $defaults ) {
        unset($defaults['date']);
        $defaults['title']  = 'Date';
        $defaults['name']  = 'Name';
        $defaults['company']   = 'Company';
        $defaults['email'] = 'Email';
        $defaults['phone_number'] = 'Phone';
        $defaults['desired_start_date'] = 'Start Date';
        $defaults['estimated_budget'] = 'Budget';
        $defaults['status'] = 'Status';

        return $defaults;
    }

    add_action("manage_inbox_posts_custom_column",  "inbox_custom_columns");

    function inbox_custom_columns($column){
        global $post;

        switch ($column) {
           case "name":
               the_field('name', $post->ID );
               break;
           case "company":
               the_field('company', $post->ID );
               break;
            case "email":
               the_field('email', $post->ID );
               break;
            case "phone_number":
               the_field('phone_number', $post->ID );
               break;
            case "desired_start_date":
               the_field('desired_start_date', $post->ID );
               break;
            case "estimated_budget":
               the_field('estimated_budget', $post->ID );
               break;
            case "status":
               the_field('status', $post->ID );
               break;
           }
        }

        //Contct function
        function contact_insert() {          

            check_ajax_referer('contact_platform_security','security');
        
            $contactDate = date('d.m.y h:i');
            $my_cptpost_args = array(
            
                'post_title'    => $contactDate,
                'post_content' => 'Empty',
                'post_type' => 'inbox',
                'post_status'   => 'publish'
            
                );
            // insert the post into the database
            $cpt_id = wp_insert_post( $my_cptpost_args, $wp_error = false);

            update_field('name', $_POST['cname'], $cpt_id);
            update_field('title_name', $_POST['ctitle'], $cpt_id);
            update_field('company', $_POST['ccompany'], $cpt_id);
            update_field('email', $_POST['cemail'], $cpt_id);
            update_field('phone_number', $_POST['cphone'], $cpt_id);
            update_field('desired_start_date', $_POST['cdate'], $cpt_id);
            update_field('estimated_budget', $_POST['cbudget'], $cpt_id);
            update_field('message', $_POST['cmessage'], $cpt_id);
            update_field('status', 'waiting', $cpt_id);

            echo json_encode(array('status' => 'ok'));

            die(''); 
        }

        add_action( 'wp_ajax_contact_insert', 'contact_insert' );    // If called from admin panel
        add_action( 'wp_ajax_nopriv_contact_insert', 'contact_insert' );


        // Hi's
        function hi_insert() {          
            check_ajax_referer('hi_platform_security','security');

            $my_cptpost_args = array(
                'post_title'    => 'Hi',
                'post_content' => 'Empty',
                'post_type' => 'hi',
                'post_status'   => 'publish'
            );
            // insert the post into the database
            $cpt_id = wp_insert_post( $my_cptpost_args, $wp_error = false);

            echo json_encode(array('status' => 'ok'));

            die(''); 
        }

        add_action( 'wp_ajax_hi_insert', 'hi_insert' );    // If called from admin panel
        add_action( 'wp_ajax_nopriv_hi_insert', 'hi_insert' );

        // Social Media Netwotks
        add_filter('manage_socialmedia_posts_columns', 'bs_socialmedia_table_head');
        function bs_socialmedia_table_head( $defaults ) {
            unset($defaults['date']);
            $defaults['title']  = 'Network';
            return $defaults;
        }

?>