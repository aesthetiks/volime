<div class="info-modal">
            <span class="close-modal">X</span>
            <div class="info-modal-icon mail"></div>
            <h2 class="info-modal-header"></h2>
            <p class="info-modal-text"></p>

            <div class="info-modal-footer">
                <?php 
                $mainService = new WP_Query(array(
                    'post_type' => 'socialmedia'
                ));

                while($mainService->have_posts()){
                    $mainService->the_post(); 
                ?>
                    <a href="<?php echo get_field('profile_link');?>"><span class="service-icon si-<?php echo get_field('icon');?> hover"></span></a>
                <?php 
                }
                ?>    
            </div>
    </div>