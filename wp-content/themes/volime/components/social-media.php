<div class="social-media-links">
    <?php 
    $mainService = new WP_Query(array(
        'post_type' => 'socialmedia'
    ));

    while($mainService->have_posts()){
        $mainService->the_post(); 
    ?>
        <a href="<?php echo get_field('profile_link');?>"><span class="icon si-<?php echo get_field('icon');?> hover"></span></a>
    <?php 
    }
    ?>    
</div>