<?php 
    $orgTitle = get_field('title');
    $title = str_replace(' ', '', $orgTitle);
?>
         <div class="service-modal <?php echo strtolower($title)?> ">
         <span class="close-modal">X</span>
             <div id="serviceInquiry_<?php echo strtolower($title)?>">
                 <div class="service-modal-header">
                     <h4>You've selected the <span name="spackage"><?php echo get_field('title');?></span> package</h4>
                     
                 </div>

                 <div class="service-modal-content">
                     <div class="service-modal-content-details">
                         <h5>Selected package includes:</h5>
                         <ul class="service-list mt44">
                         <?php 
                             for ($x = 1; $x < 7; $x++){
                                 if(get_field('item_'.$x) != ""){
                         ?>
                                 <li><p><?php echo get_field('item_'.$x);?></p></li>
                         <?php
                                 }
                             }
                         ?>
                         </ul>

                         <div class="price-group">
                             <span>$<span class="price" name="sprice"><?php echo get_field('price');?></span>/<span class="price-value"><?php echo get_field('price_value');?></span></span>
                         </div>
                     </div>
                     <div class="service-modal-content-form">
                         <h5>Leave us your details:</h5>
                         <div>
                            <input type="text" placeholder="Name" name="sname" class="sname">
                            <span class="error-message">Field is required</span>
                         </div>
                         <div>
                            <input type="text" placeholder="Email" name="semail" class="semail">
                            <span class="error-message">Field is required</span>
                         </div>
                         <div>
                            <textarea placeholder="Message" name="smessage"class="smessage"></textarea>
                            <span class="error-message">Field is required</span>
                         </div>
                         <input type="hidden" name="sprice" class="sprice" value="<?php echo get_field('price');?>" />
                         <input type="hidden" name="spackage" class="spackage" value="<?php echo get_field('title');?>" />
                     </div>
                 </div>

                 <div class="service-modal-footer">
                     <div class="buttons">
                        <button type="button" class="cancel-btn">Cancel</button>
                        <button type="button" class="submit-btn">Submit</button>
                     </div>
                 </div>  
             </div>      
         </div>
