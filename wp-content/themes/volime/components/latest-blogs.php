<div class="tagline-panel box b-top h-auto">
    <h2 class="main-text mb100 lbp-desktop">Our Latest Blog Posts</h2>
    <h2 class="main-text mb100 lbp-mobile">Our Latest Blog Post</h2>
    <ul class="latest-blog-posts">
    <?php 
        $homepageBlogs = new WP_Query(array(
            'posts_per_page' => 5,
            'post_type' => 'post'
        ));

        while($homepageBlogs->have_posts()){
            $homepageBlogs->the_post(); 
    ?>
        <li class="latest-blog-post">
            <div class="latest-blog-post-cover" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID(),'thumbnail') ?>') no-repeat; "></div>
            <div class="latest-blog-header">
                <h4><?php the_title()?></h4>
                <span><?php the_category( ' ' ); ?></span>
            </div>
            <span class="latest-blog-post-date"><?php the_date();?></span>

            <p class="latest-blog-post-text">
                <?php                                 
                    echo mb_strimwidth((trim(get_the_excerpt())), 0, 200, "...");
                ?>
            </p>

            <a href="<?php the_permalink()?>" class="story-user-link latest-blog-post-link">READ MORE</a>
        </li>   
    <?php
    } wp_reset_postdata();
    ?> 
    </ul>
</div>