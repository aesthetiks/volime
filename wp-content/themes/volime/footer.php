    <footer class="box">
        <div class="wp20">
            <a href="<?php echo site_url() ?>" class="logo"><h2 class="normal primary">Volime</h2></a>
            <div class="footer-nav">
                <?php
                    wp_nav_menu(array(
                        'theme_location' => 'footerMenu'
                    ))
                ?> 
            </div>
        </div>

        <div class="wp20">
            <h5 class="content-title mt22">Our Latest Stories</h5>
            <ul class="footer-nav mt48">
            <?php 
                $homepageStories = new WP_Query(array(
                    'posts_per_page' => 5,
                    'post_type' => 'story'
                ));

                while($homepageStories->have_posts()){
                    $homepageStories->the_post(); 
            ?>
                <li><a href="<?php the_permalink()?>" class="footer-link"><?php the_title()?></a></li>   
            <?php
            } wp_reset_postdata();
            ?> 
            </ul>
        </div>

        <div class="wp20">
            <h5 class="content-title mt22">Our Latest Blog Posts</h5>
            <ul class="footer-nav mt48">
            <?php 
                $homepageBlogs = new WP_Query(array(
                    'posts_per_page' => 5,
                    'post_type' => 'post'
                ));

                while($homepageBlogs->have_posts()){
                    $homepageBlogs->the_post(); 
            ?>
                <li><a href="<?php the_permalink()?>" class="footer-link"><?php the_title()?></a></li>   
            <?php
            } wp_reset_postdata();
            ?> 
            </ul>
        </div>

        <div class="wp15">
            <h5 class="content-title mt22">Hi's received</h5>

            <?php 
                $hi = new WP_Query(array(
                    'post_type' => 'hi',
                    'posts_per_page' => '-1'
                ));
            ?>
             <p class="hi-count mt44"><?php echo $hi->post_count; ?></p>
        </div>

        <div class="wp25 hide-mobile">
        <h5 class="content-title mt22">Newsletters & Updates</h5>
            <div class="ms0 mt48">
                 <?php
                    include(locate_template('components/subscribe.php'));  
                ?>   
            </div>
        </div>

        <p class="copyright">Volime © 2019 - All Rights Reserved</p>

    </footer>

    <?php
        include(locate_template('components/info-modal.php'));  
    ?>
    <button class="mobile-subscribe-btn"></button>
   
    <?php wp_footer(); ?>   
    </body>
</html>