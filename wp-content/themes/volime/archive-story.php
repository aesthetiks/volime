<?php 
    get_header();
?>

<div class="blog-hero"></div>
<div class="single-story-box">
<h1 class="archive-title">The Stories we have told</h1>

    <?php 
        while(have_posts()){
        the_post(); 
    ?>
            <div class="blog-box story">
                <a href="<?php the_permalink(); ?>">
                    <div class="blog-thumbnail" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID(),'thumbnail') ?>') no-repeat; ">
                    </div>
                    <div class="blog-content">
                        <h4 class="content-title"><?php the_title(); ?></h4>
                        <div class="content-text blog">
                            <?php
                                echo mb_strimwidth((trim(get_field('intro'))), 0, 352, "...");
                            ?>
                        </div>
                        <span class="read-more">Read More</span>  
                    </div>
                </a>
            </div>
    <?php
        }
        ?>
        <div class="pagination">
        <?php
        echo paginate_links();
    ?>
    </div>
</div>

<?php
    get_footer();
?>