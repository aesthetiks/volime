<?php 
    //Page custom post types
    function volime_post_types(){
        register_post_type('tagline', array(
            'supports' => array('title'),
            'public' => true,
            'exclude_from_search' => true,
            'publicly_queryable' => false,
            'show_ui' => true,
            'labels' => array(
                'name' => 'Tagline',
                'add_new_item' => 'Add new Tagline',
                'edit_item' => 'Edit Tagline',
                'all_items' => 'All Taglines', 
                'singular_name' => 'Tagline'
            ),
            'menu_icon' => 'dashicons-text',
        ));

        register_post_type('main-service', array(
            'supports' => array('title'),
            'public' => true,
            'exclude_from_search' => true,
            'publicly_queryable' => false,
            'show_ui' => true,
            'labels' => array(
                'name' => 'Main Service',
                'add_new_item' => 'Add new Main Service',
                'edit_item' => 'Edit Main Service',
                'all_items' => 'All Main Services', 
                'singular_name' => 'Main Service'
            ),
            'menu_icon' => 'dashicons-portfolio'
        ));

        register_post_type('services', array(
            'supports' => array('title'),
            'public' => true,
            'exclude_from_search' => true,
            'publicly_queryable' => false,
            'show_ui' => true,
            'labels' => array(
                'name' => 'Services',
                'add_new_item' => 'Add new Service',
                'edit_item' => 'Edit Service',
                'all_items' => 'All Services', 
                'singular_name' => 'Service'
            ),
            'menu_icon' => 'dashicons-clipboard'
        ));

        register_post_type('story', array(
            'rewrite' => array('slug' => 'stories'),
            'supports' => array('title', 'thumbnail'),
            'public' => true,
            'show_ui' => true,
            'has_archive' => true,
            'labels' => array(
                'name' => 'Stories',
                'add_new_item' => 'Add new Story',
                'edit_item' => 'Edit Story',
                'all_items' => 'All Stories', 
                'singular_name' => 'Story'
            ),
            'menu_icon' => 'dashicons-layout',
            'show_in_rest'       => true,
  		    'rest_base'          => 'stories-api',
  		    'rest_controller_class' => 'WP_REST_Posts_Controller'
        ));

        register_post_type('inquiries', array(
            'supports' => array('title'),
            'public' => true,
            'exclude_from_search' => true,
            'publicly_queryable' => false,
            'show_ui' => true,
            'labels' => array(
                'name' => 'Inquiry',
                'add_new_item' => 'Add new Inquiry',
                'edit_item' => 'Edit Inquiry',
                'all_items' => 'All Inquiries', 
                'singular_name' => 'Inquiry'
            ),
            'menu_icon' => 'dashicons-admin-comments',
        ));

        register_post_type('inbox', array(
            'supports' => array('title'),
            'public' => true,
            'exclude_from_search' => true,
            'publicly_queryable' => false,
            'show_ui' => true,
            'labels' => array(
                'name' => 'Inbox',
                'add_new_item' => 'Add new Inbox',
                'edit_item' => 'Edit Inbox',
                'all_items' => 'All Inbox', 
                'singular_name' => 'Inbox'
            ),
            'menu_icon' => 'dashicons-email-alt',
        ));

        register_post_type('socialmedia', array(
            'supports' => array('title'),
            'public' => true,
            'exclude_from_search' => true,
            'publicly_queryable' => false,
            'show_ui' => true,
            'labels' => array(
                'name' => 'Social Media',
                'add_new_item' => 'Add new Social Media',
                'edit_item' => 'Edit Social Media',
                'all_items' => 'All Social Media', 
                'singular_name' => 'Social Media'
            ),
            'menu_icon' => 'dashicons-share',
        ));

        register_post_type('hi', array(
            'supports' => array('title'),
            'public' => true,
            'exclude_from_search' => true,
            'publicly_queryable' => false,
            'show_ui' => false,
            'labels' => array(
                'name' => 'Hi',
                'add_new_item' => 'Add new Hi',
                'edit_item' => 'Edit Hi',
                'all_items' => 'All Hi', 
                'singular_name' => 'Hi'
            ),
             'capabilities' => array(
                'create_posts' => false, 
             ),
            'menu_icon' => 'dashicons-heart',
        ));

    }

    add_action('init', 'volime_post_types');


    add_action( 'rest_api_init', 'create_story_api' );

    function create_story_api() {
        register_rest_field( 'story', 'story_details', array(
            'get_callback'    => 'get_post_meta_for_api',
            'schema'          => null,
            )
        );
    }

    function get_post_meta_for_api( $object ) {
        $post_id = $object['id'];
        return get_post_meta( $post_id );
    }

?>