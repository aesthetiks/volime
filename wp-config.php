<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //

define('WP_CACHE', true);
define( 'WPCACHEHOME', '/app/public/wp-content/plugins/wp-super-cache/' );

if(file_exists(dirname(__FILE__) . '/local.php')){
	// Local
	define( 'DB_NAME', 'local' );
	define( 'DB_USER', 'root' );
	define( 'DB_PASSWORD', 'root' );
	define( 'DB_HOST', 'localhost' );
}else{
	// Live
	define( 'DB_NAME', 'volialiy_agency' );
	define( 'DB_USER', 'volialiy_volime' );
	define( 'DB_PASSWORD', '?&aK&=br(0&7' );
	define( 'DB_HOST', 'localhost' );
}


/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'nJKPYejgCPBsmpcKeOXlluwtEHd31Nqz0B7tsds+8BbV094isfsWTqCM1JYeQeVBIMdVqUB1oMJpI5X6OMt1UA==');
define('SECURE_AUTH_KEY',  '/Yla27gXxCpBZ8TwsBf2nmjzwRbeQvVDvQAqlpAU/3epVYLPf6jRboRQjfBYGPT1WnFxaetCmhtf+LVyQLnZIA==');
define('LOGGED_IN_KEY',    'zI9gvNKmgQHcV+plptuVxtnp6gZZ/7j48L5NqwjKJI6pbstm8KI3pmPEM5LNmpmI4PhTIJiXrzklQDBWfV0eGw==');
define('NONCE_KEY',        'jSSSQTe/kVGBatcFWrdDHg4lLt6ZPsBDv4CMOMUwlbI4k0P2mYzebX16i0BvBmGA64LQMm1iFIC0x3G0Cb1vAw==');
define('AUTH_SALT',        'aK5x16H+BjxAem4Ld0Yzzy/ZL9gbocC8w93pjBYaxXHADPdrP3SwMk7FJCOQKi/kXl90mNqUcLaHth5uhLm1FQ==');
define('SECURE_AUTH_SALT', 'xiB1gmKwyl2dK6GztNtzAH/+oSkN8Q71O2slQcODrQUETRQSF8NmWdQ2rAupTxm7JZNBqpq/318gVBXA9dFyHw==');
define('LOGGED_IN_SALT',   'n/dCXPX7K0KmrrSpP0yyPE+Awka6SyQOg6HQb1gtWM3rzSISCEkf+UbB7tM4h5pIwKXzrBDeIaOI+W2ttZDP5A==');
define('NONCE_SALT',       'OQ2gffiyWevI3kUK2SxOlH3dyVITPsUZzbvT3RYbuoVF6ny0W4qnJH1q+mJ/VVZJIX5uLShpATYg9OFOYLeaJQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
